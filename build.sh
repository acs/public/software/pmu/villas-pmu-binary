#!/bin/bash

GIT_REF=${GIT_REF:-master}
MAKE_THREADS=${MAKE_THREADS:-2}

SRC_FOLDER="$(mktemp --directory /tmp/villas-git.XXXXXX)"
APP_FOLDER="$(mktemp --directory /tmp/villas-app.XXXXXX)"

echo "Compile VILLAS branch ${GIT_REF} on ${MAKE_THREADS} threads"

function _exit() {
    rm -rf ${SRC_FOLDER}
}
#trap _exit EXIT

export PREFIX="${APP_FOLDER}/build"
export CONFIGURE_OPTS="--prefix=${PREFIX}"

mkdir -p ${PREFIX}
pushd ${SRC_FOLDER}

git clone --branch ${GIT_REF} https://git.rwth-aachen.de/acs/public/villas/node.git villas

pushd villas

git submodule update --init common

# Install deps needed for building
sudo apt install -y \
    libreadline-dev \
    build-essential \
    cmake \
    libjansson-dev \
    libssl-dev \
    pkg-config \
    libwebsockets-dev \
    uuid-dev \
    libre-dev \
    libcurl4-openssl-dev \
    libfmt-dev \
    libconfig-dev \
    zip

# Install additional deps of villas
TRIPLET=aarch64-linux-gnu \
SKIP_ETHERLAB=1 \
SKIP_FMTLIB=1 \
SKIP_CRITERION=1 \
SKIP_LIBIEC61850=1 \
SKIP_WEBSOCKETS=1 \
SKIP_RDKAFKA=1 \
SKIP_COMEDILIB=1 \
SKIP_LIBRE=1 \
SKIP_NANOMSG=1 \
SKIP_LIBXIL=1 \
SKIP_REDIS=1 \
MAKE_THREADS=${MAKE_THREADS} \
SKIP_LDCONFIG=1 \
SKIP_GRAPHVIZ=1 \
SKIP_LUA=1 \
bash packaging/deps.sh

mkdir -p build
pushd build

cmake   -DCMAKE_BUILD_TYPE=Debug \
        -DWITH_FPGA=OFF \
	-DWITH_GRAPHVIZ=OFF \
        -DWITH_NODE_WEBSOCKET=OFF \
       	-DWITH_NODE_REDIS=OFF \
       	-DWITH_LUA=OFF \
	-DCMAKE_INSTALL_PREFIX=${PREFIX} \
	-DCMAKE_PREFIX_PATH=${PREFIX} ..

make -j${MAKE_THREADS} install

cd ${APP_FOLDER}/build
zip -r /app/villas.zip .
echo "Folder with villas.zip ${APP_FOLDER}/build"
